## Openbox themes

A collection of few themes for Openbox window manager

### Themes

- Materia (based on [Adapta Project work](https://github.com/adapta-project/adapta-gtk-theme))
- Fluent (based on [Windows 10 Transformation Pack Dark](https://github.com/B00merang-Project/Windows-10-Dark))